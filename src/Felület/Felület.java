package Felület;
import Dolgozik.Dolgozik;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Formatter;
import java.util.Scanner;

import javax.swing.*;
import java.awt.*;

public class Felület extends JFrame {

    private Formatter x;
    private Scanner scan;
    private JTextArea jTextArea;
    private JButton jButtonSave;
    private JButton jButtonOpen;
    private JButton jButtonTöröl;
    private JScrollPane scrolltxt;

    public Felület(){
        jTextArea = new JTextArea();
        scrolltxt = new JScrollPane(jTextArea);



        setLayout(null);

        scrolltxt.setBounds(4, 4, 427, 300);
            add(scrolltxt);

        jButtonSave = new JButton("Mentés");
        jButtonSave.setBounds(4,315,120,30);
        add(jButtonSave);

        jButtonOpen = new JButton("Megnyitás");
        jButtonOpen.setBounds(310,315,120,30);
        add(jButtonOpen);

        jButtonTöröl = new JButton("Törlés");
        jButtonTöröl.setBounds(160,315,120,30);
        add(jButtonTöröl);

        jButtonSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                folder("Jegyzet");
                perform();
            }
        });

        jButtonOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
               fcs();

            }
        });

        jButtonTöröl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                jTextArea.setText(null);
            }
        });


    }

    public void perform(){
        open();
        format();
        close();
    }


    public void open(){

        String n = próba();

        if(!n.isEmpty()){
            try{
                x=new Formatter("C:\\Jegyzet\\"+n+".txt");
            } catch (FileNotFoundException e) {}
            JOptionPane.showMessageDialog(null, "A dokumentum elkészült\n Mentve: C:\\Jegyzet");}
        else {JOptionPane.showMessageDialog(null, "Nem adott meg nevet!");}}

    public void format(){
        x.format("%s", jTextArea.getText());
    }

    public void close(){
        x.close();
    };

    public String folder(String jegyzet) {
        try {
            String dir = "C:\\" + jegyzet;
            File directory = new File(dir);

            if (!directory.exists()) {
                directory.mkdir();
                System.out.println("Folder is created");

            } else {
                System.out.println("Hiba, vagy már van ilyen mappa");
            }

        }catch(Exception e) {
            e.printStackTrace();
        }return " ";}

    public String próba(){
        String n = JOptionPane.showInputDialog("Milyen néven szeretné menteni a file-t?");
        try{
            scan = new Scanner(new File("C:\\Jegyzet\\"+n));
            JOptionPane.showMessageDialog(null,"A file név már létezik. Próbálja újra!");

            File folder = new File("C:\\Jegyzet\\");
            File [] listOfFiles = folder.listFiles();

            JOptionPane.showMessageDialog(null,listOfFiles);

            close();
        }catch(FileNotFoundException e){System.out.println("Nincs ilyen");}
        return n;
    }

    public void fcs() {
        try
        {
            JFileChooser open = new JFileChooser("C:\\Jegyzet\\");
            int option = open.showOpenDialog(this);
            File f1 = new File(open.getSelectedFile().getPath());
            FileReader fr = new FileReader(f1);
            BufferedReader br = new BufferedReader(fr);
            String s;
            while((s=br.readLine())!=null)
            {
                jTextArea.append(s + "\n");
            }
            fr.close();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }

    }
}



